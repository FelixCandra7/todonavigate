import React  from 'react';
import { StyleSheet, Text, View , Button , TextInput,
         FlatList, ScrollView, Switch } from 'react-native';
import { createStackNavigator, createAppContainer} from 'react-navigation';
import  DateTimePicker from 'react-native-modal-datetime-picker';

class HomeScreen extends React.Component {
  
  state = {
    switchValue: false,
    toDoItem: [],
  }

  setData = async () => {
    const arrayIndex = this.state.toDoItem.length;
    var PickedDate;
    var PickedTime;
      if(this.props.navigation.getParam('month').length<2
         && this.props.navigation.getParam('date') .length<2){
          PickedDate = '0' + this.props.navigation.getParam('date') 
          + ' / ' + '0' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      }
      else if(this.props.navigation.getParam('month').length<2
        && this.props.navigation.getParam('date').length==2){
          PickedDate = '0' + this.props.navigation.getParam('date')  
          + ' / ' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      } 
      else if(this.props.navigation.getParam('month')==2
        && this.props.navigation.getParam('date').length<2){
          PickedDate = this.props.navigation.getParam('date') 
          + ' / ' + '0' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      } 
      else{
        PickedDate = this.props.navigation.getParam('date') 
        + ' / ' + this.props.navigation.getParam('month') 
        + ' / ' + this.props.navigation.getParam('year') ;
      }

      if(this.props.navigation.getParam('hour').length<2
         && this.props.navigation.getParam('minute').length<2){
        PickedTime = "0" + this.props.navigation.getParam('hour') 
        + '.' + '0' + this.props.navigation.getParam('minute'); 
      }
      else if(this.props.navigation.getParam('hour').length<2
         && this.props.navigation.getParam('minute').length==2){
        PickedTime = '0' + this.props.navigation.getParam('hour')  
        + '.' + this.props.navigation.getParam('minute'); 
      } 
      else if(this.props.navigation.getParam('hour').length==2
         && this.props.navigation.getParam('minute').length<2){
        PickedTime = this.props.navigation.getParam('hour') 
        + '.' + '0' + this.props.navigation.getParam('minute'); 
      } 
      else{
        PickedTime = this.props.navigation.getParam('hour') 
        + '.' + this.props.navigation.getParam('minute');
      }

    this.setState(() => ({
      toDoItem: [...this.state.toDoItem,{
          taskId: arrayIndex,
          title: this.props.navigation.getParam('title'),
          description: this.props.navigation.getParam('description'),
          switchValue: false,
          date: PickedDate,
          time: PickedTime,
      }],
    }));
    
  }

  toggleSwitch = (item) =>{
    const newArray = this.state.toDoItem.map((toDoCopy) => {
      if (toDoCopy.taskId === item.taskId) {
        return ({
          description: toDoCopy.description,
          date: toDoCopy.date,
          time: toDoCopy.time,
          title: toDoCopy.title,
          switchValue: !toDoCopy.switchValue,
          taskId: toDoCopy.taskId,
        });
      }
      return toDoCopy;
    });
    this.setState({ toDoItem: newArray });
  };

  _renderItem = ({item}) => (
    <View style={styles.toDoGrid}>
      <Text style={styles.itemTitle}>{item.title}</Text>
      <View style={styles.switchDate}>
        <Switch 
          onValueChange={() => this.toggleSwitch(item)} 
          value={item.switchValue}>
        </Switch>
        <Text>{item.date} {item.time}</Text>
      </View>
      <Text>description: {item.description}</Text>
    </View>
  );

  render(){
    const {state} = this.props.navigation;
    const params = state.params || {};
    params.setData;

    return(
      <View>
        <View style={styles.header}>
          <Text style={styles.title}>To Do List</Text>
          <Button
            title="Add"
            color="#bf4900"
            onPress={() => this.props.navigation.navigate('Add')}
          />

          <Button
            title="Set"
            color="#bf4900"
            onPress={this.setData}
          />
        </View>

        <View style={styles.listView}>
          <FlatList
            data = {this.state.toDoItem}
            keyExtractor = {item => item.taskId.toString()}
            renderItem = {this._renderItem}
          />
        </View>   

      </View>
    );
  }
}

class AddItemScreen extends React.Component{
  constructor(props){
    super(props);
    this.setData = this.setData.bind(this);
  }

  state={
    title: "",
    description: "",
    year: "",
    month: "",
    date: "",
    hour: "",
    minute: "",
    isDatePickerVisible: false,
    isTimePickerVisible: false,
  };

  showDatePicker = () => {
    this.setState({isDatePickerVisible: true});
  };
  
  hideDatePicker = () => {
    this.setState({isDatePickerVisible: false});
  };
  
  handleDatePicked = data => {
    this.setState({
      year: data.getFullYear(),
      month: data.getMonth().toString(),
      date: data.getDate().toString(),
      hour: data.getHours().toString(),
      minute: data.getMinutes().toString()
    });
    
    this.hideDatePicker();
  }

  navigateToHome = () => {
    this.props.navigation.navigate('Home',{
    title: this.state.title,
    description: this.state.description,
    year: this.state.year,
    month: this.state.month,
    date: this.state.date,
    hour: this.state.hour,
    minute: this.state.minute,
    setData: this.setData,
    });  
  }

  setData = async () => {
    const arrayIndex = this.state.toDoItem.length;
    var PickedDate;
    var PickedTime;
      if(this.props.navigation.getParam('month').length<2
         && this.props.navigation.getParam('date') .length<2){
          PickedDate = '0' + this.props.navigation.getParam('date') 
          + ' / ' + '0' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      }
      else if(this.props.navigation.getParam('month').length<2
        && this.props.navigation.getParam('date').length==2){
          PickedDate = '0' + this.props.navigation.getParam('date')  
          + ' / ' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      } 
      else if(this.props.navigation.getParam('month')==2
        && this.props.navigation.getParam('date').length<2){
          PickedDate = this.props.navigation.getParam('date') 
          + ' / ' + '0' + this.props.navigation.getParam('month') 
          + ' / ' + this.props.navigation.getParam('year') ;
      } 
      else{
        PickedDate = this.props.navigation.getParam('date') 
        + ' / ' + this.props.navigation.getParam('month') 
        + ' / ' + this.props.navigation.getParam('year') ;
      }

      if(this.props.navigation.getParam('hour').length<2
         && this.props.navigation.getParam('minute').length<2){
        PickedTime = "0" + this.props.navigation.getParam('hour') 
        + '.' + '0' + this.props.navigation.getParam('minute'); 
      }
      else if(this.props.navigation.getParam('hour')<2
         && this.props.navigation.getParam('minute').length==2){
        PickedTime = '0' + this.props.navigation.getParam('hour')  
        + '.' + this.props.navigation.getParam('minute'); 
      } 
      else if(this.props.navigation.getParam('hour').length==2
         && this.props.navigation.getParam('minute').length<2){
        PickedTime = this.props.navigation.getParam('hour') 
        + '.' + '0' + this.props.navigation.getParam('minute'); 
      } 
      else{
        PickedTime = this.props.navigation.getParam('hour') 
        + '.' + this.props.navigation.getParam('minute');
      }

    this.setState(() => ({
      toDoItem: [...this.state.toDoItem,{
          taskId: arrayIndex,
          title: this.props.navigation.getParam('title'),
          description: this.props.navigation.getParam('description'),
          switchValue: false,
          date: PickedDate,
          time: PickedTime,
      }],
    })); 
  }

  render(){
    var date;
      var time;
      if(this.state.month.length<2&&this.state.date.length<2){
        date = '0' + this.state.date + ' / ' + '0' + this.state.month + ' / ' + this.state.year ;
      }else if(this.state.month.length<2&&this.state.date.length==2){
        date = '0' + this.state.date + ' / ' + this.state.month + ' / ' + this.state.year ;
      } else if(this.state.month.length==2&&this.state.date.length<2){
        date = this.state.date + ' / ' + '0' + this.state.month + ' / ' + this.state.year ;
      } else{
        date = this.state.date + ' / ' + this.state.month + ' / ' + this.state.year ;
      }

      if(this.state.hour.length<2&&this.state.minute.length<2){
        time = "0" + this.state.hour + '.' + '0' + this.state.minute; 
      }else if(this.state.hour.length<2&&this.state.minute.length==2){
        time = '0' + this.state.hour + '.' + this.state.minute; 
      } else if(this.state.hour.length==2&&this.state.minute.length<2){
        time = this.state.hour + '.' + '0' + this.state.minute; 
      } else{
        time = this.state.hour + '.' + this.state.minute;
      }

    return(
      <View style={styles.formContainer}>
        <View style={styles.header}>
          <Button
            title="Back"
            color="#bf4900"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>

        <ScrollView style={styles.form}>
          <View style={styles.titleGrid}>
            <Text style={styles.itemTitle}>Title</Text>
            <TextInput
              style={styles.titleBox} 
              placeholder="Write the title here"
              placeholderTextColor="#000000"
              onChangeText={(title) => this.setState({title: title})}
              value={this.state.title}
            /> 
          </View>

          <View style={styles.dateGrid}>
            <Text style={styles.itemTitle}>When</Text>
            <Button 
                title="Select Date"
                onPress={this.showDatePicker}
                style={styles.dateButton}
            />
            <DateTimePicker
                mode="datetime"
                isVisible={this.state.isDatePickerVisible}
                onConfirm={this.handleDatePicked}
                onCancel={this.hideDatePicker}
            /> 
            <Text style={styles.dateTimeText}>Selected Date (D/M/Y): {date} Selected Time: {time}</Text> 
          </View>

          <View style={styles.descriptionGrid}>
            <Text style={styles.itemTitle}>Description</Text>
            <TextInput
              style={styles.descriptionBox} 
              placeholder="Write the description here"
              placeholderTextColor="#000000"
              onChangeText={(description) => this.setState({description: description})}
              value={this.state.description}
              multiline={true}
              numberOfLines={5}
              textAlignVertical={'top'}
            />  
          </View>

          <View style={styles.submitGrid}>
            <Button 
                title="Add Item"
                onPress={this.navigateToHome}
                style={styles.dateButton}
            />
          </View>
        </ScrollView>
      </View> 
    );
  }
}

const MainStack = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  Add: {
    screen: AddItemScreen
  }
},{
  initialRouteName: "Home",
  headerMode: 'none',
});


const AppContainer = createAppContainer(MainStack);

export default class App extends React.Component {
  render() {
    return <AppContainer/>;
  }
}

const styles = StyleSheet.create({
  header:{
    height: 80,
    backgroundColor: '#f4511e',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  title:{
    color: 'white',
    fontWeight: 'bold',
    fontSize: 30,
  },
  itemTitle:{
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 20,
  },
  listView:{
    height: 600,
  },
  form:{
    flex:1,
  },
  titleGrid:{
    height: 120,
    padding: 10,
  },
  itemTitle:{
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10,
  },
  titleBox:{
    height: 60,
    backgroundColor: "#fcfaf4",
    borderColor: '#c9c9c9',
    borderWidth: 1,
    paddingLeft: 20,
  },
  dateGrid:{
    height:120,
    backgroundColor:'white',
    padding: 10,
  },
  dateTimeText:{
    paddingTop: 10,
  },
  descriptionGrid:{
    height: 200,
    backgroundColor: "white",
    padding: 10,
  },
  descriptionBox:{
    height: 140,
    backgroundColor: "#fcfaf4",
    flexDirection: 'row',
    alignItems: 'flex-start',
    borderColor: 'white',
    borderWidth: 1,
    borderColor: '#c9c9c9',
    paddingLeft: 20,
  },
  formContainer: {
    flex:1,
  },
  submitGrid:{
    height: 60,
    padding: 10,
  },
  toDoGrid:{
    height: 100,
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingLeft: 20,
  },
  switchDate:{
    flexDirection: "row",
    alignItems:'flex-start',
  }
});
